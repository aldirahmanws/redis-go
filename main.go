package main

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type User struct {
	Name      string `json:"name"`
	CreatedAt int    `json:"created_at"`
}

type UserInput struct {
	Id        string `json:"id" binding:"required"`
	Name      string `json:"name" binding:"required"`
	CreatedAt int    `json:"created_at" binding:"required"`
}

func main() {
	router := gin.Default()

	router.POST("/user/save", func(c *gin.Context) {
		var input UserInput

		err := c.ShouldBindJSON(&input)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": true, "message": "Input not valid"})
			return
		}
		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})

		_, err = client.Get(input.Id).Result()
		if err == nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": true, "message": "Already saved in redis"})
			return
		}

		json, err := json.Marshal(User{Name: input.Name, CreatedAt: input.CreatedAt})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": true, "message": "Error"})
			return
		}

		err = client.Set(input.Id, json, 0).Err()
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": true, "message": "Error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"error": 0, "message": ""})
	})
	router.GET("/user/detail/:id", func(c *gin.Context) {
		id := c.Param("id")

		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})

		val, err := client.Get(id).Result()
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": true, "message": "ID not found"})
			return
		}

		var user User
		err = json.Unmarshal([]byte(val), &user)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": true, "message": "Error"})
			return
		}

		data := User{Name: user.Name, CreatedAt: user.CreatedAt}
		c.JSON(http.StatusOK, gin.H{"error": 0, "message": "", "data": data})
	})

	router.Run("localhost:8080")

}
